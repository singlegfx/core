#include "events/eventSystem.hpp"

#include <utility>

using namespace std::literals;

// event poller implementation

// event handle implementation

// event manager implementation
SimpleGFX::eventManager::eventManager(std::shared_ptr<logger> _loggingInterface){

    loggingInterface = (_loggingInterface) ? std::move(_loggingInterface) : std::make_shared<logger>(loggingLevel::debug);

    eventThread = std::async([this]() {
        std::vector<std::shared_ptr<trackedPromise<bool>>> unreceivedFutures;

        while (true) {
            // poll for new events
            poll();

            // move unhandled events into events
            std::vector<std::pair<inputEvent, std::shared_ptr<trackedPromise<bool>>>> events;
            {
                std::scoped_lock lock{eventMutex};
                for (auto i = unhandledEvents.begin(); i < unhandledEvents.end();) {
                    events.emplace_back(*i);
                    i = unhandledEvents.erase(i);
                }
            }

            // handle all events
            for (auto i = events.begin(); i < events.end();) {
                bool handledEvent = false;
                eventHandleSignal.emit(i->first, handledEvent);

                i->second->set_value(handledEvent);
                unreceivedFutures.emplace_back(i->second);
                i = events.erase(i);
            }

            // clean up the unreceived futures
            for (auto i = unreceivedFutures.begin(); i < unreceivedFutures.end();) {
                if ((*i)->is_read()) {
                    i = unreceivedFutures.erase(i);
                }else{
                    i++;
                }
            }

            // check if the thread should exit
            // after quitting is set to true, this should run
            // until all events are handled
            if (quitting) {
                std::shared_lock lock{eventMutex};
                if (unhandledEvents.empty()) {
                    *loggingInterface << loggingLevel::debug << "stopping after " << unhandledEvents.size() << " events are handled";
                    return;
                }
            }

            // sleep a bit before checking again
            std::this_thread::sleep_for(1ms);
        }
    });
}

SimpleGFX::eventManager::~eventManager() {
    quitting = true;

    if (eventThread.valid()) {
        eventThread.wait();
    }
}

[[maybe_unused]]
std::shared_ptr<SimpleGFX::trackedFuture<bool>> SimpleGFX::eventManager::raiseEvent(const inputEvent& event) {
    if (quitting) {
        throw std::runtime_error("cannot raise event while quitting");
    }

    std::scoped_lock lock{eventMutex};
    auto             promise = trackedPromise<bool>::create();
    unhandledEvents.emplace_back(event, promise);
    return promise->get_future();
}

[[maybe_unused]]
std::vector<std::shared_ptr<SimpleGFX::trackedFuture<bool>>> SimpleGFX::eventManager::raiseEvents(const std::vector<inputEvent>& events){
    if (quitting) {
        throw std::runtime_error("cannot raise event while quitting");
    }

    std::vector<std::shared_ptr<trackedFuture<bool>>> futures;
    futures.reserve(events.size());

    std::scoped_lock lock{eventMutex};
    for(const auto& event:events){
        auto promise = trackedPromise<bool>::create();
        unhandledEvents.emplace_back(event, promise);
        futures.emplace_back(promise->get_future());
    }
    return futures;
}

void SimpleGFX::eventManager::poll() {
    if (quitting) {
        throw std::runtime_error("cannot poll for events while quitting");
    }

    eventPollSignal.emit(*this);
}
