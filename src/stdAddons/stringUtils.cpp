#include "stdAddons/stringUtils.hpp"

#include <sstream>

std::vector<std::string> SimpleGFX::string::splitString(const std::string& s, char delimiter) {
    // Create an empty vector to hold the resulting substrings
    std::vector<std::string> tokens;
    // Create a temporary string to hold each substring as it is extracted
    std::string token;
    // Create a stream for parsing the input string
    std::istringstream tokenStream(s);
    // Loop over the input string, extracting substrings at each occurrence of
    // the delimiter character
    while (std::getline(tokenStream, token, delimiter)) {
        // Add the current substring to the vector of tokens
        tokens.push_back(token);
    }
    // Return the vector of substrings
    return tokens;
}


std::string SimpleGFX::string::removeWhitespace(const std::string& s) {
    std::string result = s;
    for (auto it = result.begin(); it != result.end();) {
        if (std::isspace(*it)) {
            it = result.erase(it);
        } else {
            ++it;
        }
    }
    return result;
}

std::string SimpleGFX::string::toLower(const std::string& s) {
    std::string result = s;
    for (auto& c : result) {
        c = std::tolower(c);
    }
    return result;
}