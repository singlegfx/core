#include "stdAddons/filesystemUtils.hpp"

#include <fstream>

std::string SimpleGFX::filesystem::loadFile(std::filesystem::path location) {
    // Create string to hold data from file
    std::string stringData;

    // Open file stream
    std::ifstream File;

    // Ensure ifstream objects can throw exceptions
    File.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        // Open file at specified location
        File.open(location);

        // Create stringstream object to store file data in buffer
        std::stringstream DataStream;

        // Read the entire file and store it in the stringstream buffer
        DataStream << File.rdbuf();

        // Close file stream
        File.close();

        // Convert the stringstream buffer to string
        stringData = DataStream.str();

    } catch (...) {
        // If an exception occurs, throw a nested exception with a helpful error message
        std::throw_with_nested(std::runtime_error("Failed to read data from file: " + location.string()));
    }

    // Return the data as a string
    return stringData;
}


std::filesystem::path SimpleGFX::filesystem::getUserDirectory() {
    constexpr const char* home_env_name =
#ifdef _WIN32
        "USERPROFILE";
#else
        "HOME";
#endif

    std::filesystem::path home{std::getenv(home_env_name)};

    if (home.empty()) {
        throw std::runtime_error("Could not find user directory");
    }

    return home;
}


std::filesystem::path SimpleGFX::filesystem::getApplicationDirectory(const std::string& appName) {
    try {
        auto home = getUserDirectory();

        std::filesystem::path appDir;

#ifdef _WIN32
        appDir = home / "AppData" / "Local" / appName;
#elif __APPLE__
        appDir = home / "Library" / "Application Support" / appName;
#else
        appDir = home / ".local" / "share" / appName;
#endif

        if (!std::filesystem::exists(appDir)) {
            std::filesystem::create_directories(appDir);
        }

        return appDir;

    } catch (...) {
        std::throw_with_nested(std::runtime_error("Error getting the app directory"));
    }
}
