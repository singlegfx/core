#include "stdAddons/exceptionUtils.hpp"

#include <iostream>
#include <sstream>

void SimpleGFX::exception::decodeException(const std::exception& e, std::stringstream& outputStream, int level) {

    // Print the exception with the appropriate indentation
    outputStream << std::string(level, ' ') << "exception: " << e.what() << '\n';

    // Try to rethrow any nested exceptions, printing them if caught
    try {
        std::rethrow_if_nested(e);
    } catch (const std::exception& nestedException) {
        // If there is a nested exception, recursively call print_exception with the nested exception and the appropriate level of
        // indentation
        decodeException(nestedException, outputStream, level + 1);
    } catch (...) {
        // Ignore any non-std::exception types; we don't care about their messages
    }
}


void SimpleGFX::exception::printException(const std::exception& e) {
    std::stringstream ss{};

    // Call the other version of print_exception with the given exception and a starting indentation level of 0
    decodeException(e, ss, 0);
    std::cerr << ss.str();
}