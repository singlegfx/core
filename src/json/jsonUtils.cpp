#include "json/jsonUtils.hpp"

SimpleGFX::json::jsonException::jsonException(int id_, std::string what_arg) : nlohmann::detail::exception{id_, what_arg.c_str()} {}

nlohmann::json SimpleGFX::json::getJsonField(const nlohmann::json& data_json, const std::string& location) {
    auto it = data_json.find(location);
    if (it == data_json.end()) {
        throw jsonException(1001, "field does not exist");
    }

    auto val = it.value();
    if (val.empty()) {
        throw jsonException(1002, "field exists but is empty");
    }

    return val;
}

std::optional<nlohmann::json>  SimpleGFX::json::getOptionalJsonField(const nlohmann::json& data_json, const std::string& location) noexcept {
    try {
        auto val = getJsonField(data_json, location);
        return val;
    } catch (...) {
        return {};
    }
}
