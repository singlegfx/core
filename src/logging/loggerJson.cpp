#include "logging/loggerJson.hpp"
#include <fstream>

SimpleGFX::loggerJson::loggerJson(loggingLevel _displayLevel, std::filesystem::path _logPath, bool _updateEveryMessage)
    : logger(_displayLevel),
      logPath(_logPath),
      updateEveryMessage(_updateEveryMessage) {
    jsonData = nlohmann::json::array();
}

SimpleGFX::loggerJson::~loggerJson() {
    if (!updateEveryMessage) {
        std::scoped_lock lock{outputBufferMutex};

        std::ofstream file{logPath};
        file << jsonData.dump(4);
    }
}

void SimpleGFX::loggerJson::logLine(const std::string& message, SimpleGFX::loggingLevel level) {
    if (level >= displayedLevel) {
        std::scoped_lock lock{outputBufferMutex};

        auto newMessage = nlohmann::json::object();
        newMessage["level"] = getLevelName(level);
        newMessage["message"] = message;
        newMessage["time"] = getCurrentTimestamp();

        jsonData.push_back(newMessage);

        if (updateEveryMessage) {
            std::ofstream file{logPath};
            file << jsonData.dump(4);
        }
    }
}