#include "logging/logger.hpp"

#include "stdAddons/typedSwitch.hpp"
#include "stdAddons/stringUtils.hpp"
#include "stdAddons/exceptionUtils.hpp"

SimpleGFX::logStream::logStream(SimpleGFX::loggingLevel _level, SimpleGFX::logger& _loggerPtr)
    : level{_level},
      loggerPtr{_loggerPtr} {}

SimpleGFX::logStream::~logStream() {
    loggerPtr.logMessage(str(), level);
}

SimpleGFX::loggingLevel SimpleGFX::nextLevel(SimpleGFX::loggingLevel level) {
    switch (level) {
        case debug:
            return detail;
        case detail:
            return warning;
        case warning:
            return normal;
        case normal:
            return error;
        case error:
            return fatal;
        case fatal:
            return fatal;
        default:
            return normal;
    }
}

SimpleGFX::loggingLevel SimpleGFX::levelFromString(const std::string& level) {
    using namespace SimpleGFX::string;

    const auto levelClean = toLower(removeWhitespace(level));
    const auto levels     = {"debug", "detail", "warning", "normal", "error", "fatal"};

    switch (SimpleGFX::TSwitch(levelClean, levels)) {
        case 0:
            return debug;
        case 1:
            return detail;
        case 2:
            return warning;
        case 3:
            return normal;
        case 4:
            return error;
        case 5:
            return fatal;
        default:
            throw std::invalid_argument("Invalid logging level: " + level);
    }
}

SimpleGFX::logger::logger(loggingLevel _displayLevel, bool _showLevel, bool _showTime)
    : displayedLevel(_displayLevel),
      showLevel(_showLevel),
      showTime(_showTime) {}

void SimpleGFX::logger::logException(const std::exception& e, bool isFatal) {
    std::stringstream ss{};
    exception::decodeException(e, ss);
    logLine(ss.str(), isFatal ? fatal : error);
}

void SimpleGFX::logger::logCurrrentException(bool isFatal) {
    auto eptr = std::current_exception();
    if (eptr) {
        try {
            std::rethrow_exception(eptr);
        } catch (const std::exception& e) {
            logException(e, isFatal);
        }
    }
}

void SimpleGFX::logger::logMessage(const std::string& message, SimpleGFX::loggingLevel level) {
    logLine(message, level);
}

void SimpleGFX::logger::logLine(const std::string& message, loggingLevel level) {
    if (level >= displayedLevel) {
        std::scoped_lock lock{outputBufferMutex};

        auto& outputStream = level >= warning ? std::cerr : std::cout;
        if (showTime) {
            outputStream << "[" << getCurrentTimestamp() << "] ";
        }
        if (showLevel) {
            outputStream << getLevelName(level) << ": ";
        }
        outputStream << message << std::endl;
    }

    logExtras(message, level);
}

SimpleGFX::logStream SimpleGFX::logger::operator<<(loggingLevel level) {
    return logStream{level, *this};
}

void SimpleGFX::logger::logExtras(const std::string& message, loggingLevel level) {
    std::shared_lock lock{extraLoggerMutex};
    for (auto& logger : extraLoggers) {
        logger->logMessage(message, level);
    }
}

void SimpleGFX::logger::addExtraLogger(std::shared_ptr<logger> logger) {
    std::scoped_lock lock{extraLoggerMutex};
    extraLoggers.emplace_back(logger);
}

void SimpleGFX::logger::removeExtraLogger(std::shared_ptr<logger> logger) {
    std::scoped_lock lock{extraLoggerMutex};
    for (auto it = extraLoggers.begin(); it != extraLoggers.end(); ++it) {
        if (*it == logger) {
            extraLoggers.erase(it);
            return;
        }
    }
}
