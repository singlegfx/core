#include "logging/loggerTxt.hpp"
#include <fstream>

SimpleGFX::loggerTxtProperties::loggerTxtProperties(SimpleGFX::loggingLevel _displayLevel, const std::filesystem::path& _logPath)
    : displayLevel{_displayLevel},
      logPath{_logPath} {}

SimpleGFX::loggerTxt::loggerTxt(SimpleGFX::loggingLevel _displayLevel, const std::filesystem::path& _logPath)
    : loggerTxt{loggerTxtProperties{_displayLevel, _logPath}} {}

SimpleGFX::loggerTxt::loggerTxt(const SimpleGFX::loggerTxtProperties& props)
    : logger(props.displayLevel, props.showLevel, props.showTime),
      logPath(props.logPath),
      overrideFile(!props.appendFile) {
    if (logPath.empty()) {
        throw std::invalid_argument("log path is empty, set the log path to use the txt logger");
    }

    if (std::filesystem::is_directory(logPath)) {
        throw std::invalid_argument("log path must be a file not a directory");
    }

    // create empty file if it does not exist or should be cleared
    if (!std::filesystem::exists(logPath) || props.cleanFile) {
        try {
            std::ofstream file{logPath, std::ios::out};
            file << "";
        } catch (...) {
            std::throw_with_nested(std::invalid_argument("Could not create clean log file for:" + logPath.string()));
        }
    }

    // write a debug message to tests if file is writeable
    try {
        logLine("testing if file is writeable", displayedLevel);
    } catch (...) {
        std::throw_with_nested(std::invalid_argument("could not log tests message to file"));
    }
}


void SimpleGFX::loggerTxt::logLine(const std::string& message, SimpleGFX::loggingLevel level) {
    if (level >= displayedLevel) {
        std::scoped_lock lock{outputBufferMutex};

        try {
            std::ofstream file{logPath, overrideFile ? std::ios::out : std::ios::app};
            if (showTime) {
                file << "[" << getCurrentTimestamp() << "] ";
            }
            if (showLevel) {
                file << getLevelName(level) << ": ";
            }
            file << message << std::endl;
        } catch (...) {
            std::throw_with_nested(std::runtime_error("Error writing to txt log file"));
        }
    }

    logExtras(message, level);
}
