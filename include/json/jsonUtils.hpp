#pragma once

#include <nlohmann/json.hpp>

#include <optional>

#ifndef SIMPLEGFX_EXPORT_MACRO
    #define SIMPLEGFX_EXPORT_MACRO
#endif

namespace SimpleGFX::json {
    /**
     * @brief A simple wrapper class around nlohmann::json::exception.
     * This allows creating and throwing json exceptions easily.
     * The reason this is needed is because the individual json exceptions are in
     * the detail namespace and are subject to change/break/remove.
     */
    class SIMPLEGFX_EXPORT_MACRO jsonException : public nlohmann::json::exception {
      public:
        /**
         * Create a json exception with the given id and what_arg
         * @param id_ The error type id of the exception
         * @param what_arg The message of the exception
         */
        jsonException(int id_, std::string what_arg);
    };

    /**
     * @brief Retrieves a specific JSON field from a JSON data object safely.
     *
     * This function retrieves the value of a specific field from a given JSON data
     * object, preventing the program from crashing if the field does not exist or
     * if it is empty. If the desired field exists but is empty, this function will
     * throw an exception.
     *
     * @param data_json A constant reference to the JSON data object.
     * @param location The name of the desired field as a string.
     * @return nlohmann::json The value of the specified field in the JSON data object.
     *
     * @throws nlohmann::json::out_of_range Thrown when the desired field does not exist in the JSON data object.
     * @throws nlohmann::json::invalid_iterator Thrown when the desired field exists but is empty in the JSON data object.
     *                                          Note that an empty array or object is considered non-empty.
     */
    SIMPLEGFX_EXPORT_MACRO nlohmann::json getJsonField(const nlohmann::json& data_json, const std::string& location);

    /**
     * @brief Retrieves a specific JSON field from a JSON data object safely.
     *
     * This function retrieves the value of a specific field from a given JSON data
     * object, and returns it as an optional. If the field does not exist or is
     * empty, the returned optional will be empty.
     *
     * @param data_json A constant reference to the JSON data object.
     * @param location The name of the desired field as a string.
     * @return std::optional<nlohmann::json> An optional that either contains the value of the specified
     *                                      field in the JSON data object, or is empty if the field does not exist or is empty.
     */
    SIMPLEGFX_EXPORT_MACRO std::optional<nlohmann::json> getOptionalJsonField(const nlohmann::json& data_json, const std::string& location) noexcept;

    /**
     * @brief Get a value from a json field
     *
     * This function returns the value of a requested field in the requested type. If the field does not exist, the function will throw
     * an exception of type nlohmann::json::other_error. If the type of the field cannot be converted to the requested type, the
     * function will throw an exception of type nlohmann::json::type_error.
     *
     * @tparam T The type the field should have
     * @param data_json The json data object
     * @param location The name of the wanted field
     * @return T The value of the json field in the requested type
     * @throws nlohmann::json::other_error If the json field does not exist
     * @throws nlohmann::json::type_error If the json field cannot be converted to the requested type
     */
    template <typename T>
    T getJsonField(const nlohmann::json& data_json, const std::string& location) {
        nlohmann::json val;
        try {
            val = getJsonField(data_json, location);
        } catch (...) {
            std::throw_with_nested(std::invalid_argument("error parsing field"));
        }

        T retval;
        try {
            retval = val.get<T>();
        } catch (...) {
            std::throw_with_nested(std::invalid_argument("wrong field type"));
        }

        return retval;
    }

    /**
     * @brief Get an optional value from json data
     *
     * This function returns the value of a requested field in the requested type, or an empty optional value if the field does not
     * exist. If the type of the field cannot be converted to the requested type, the function will throw an exception of type
     * std::exception.
     *
     * @tparam T The type the field should have
     * @param data_json The json data object
     * @param location The name of the wanted field
     * @return std::optional<T> The value of the json field in the requested type, or an empty optional value if it does not exist
     * @throws std::exception If there is an error parsing the optional field
     */
    template <typename T>
    std::optional<T> getOptionalJsonField(const nlohmann::json& data_json, const std::string& location)noexcept {
        T val;
        try {
            val = getJsonField<T>(data_json, location);
        } catch (...) {
            return {};
        }

        return val;
    }

    /**
     * @brief Get an optional value from json data
     *
     * This function returns the value of a requested field in the requested type, or a fallback value if the field does not exist. If
     * the type of the field cannot be converted to the requested type, the function will throw an exception of type std::exception.
     *
     * @tparam T The type the field should have
     * @param data_json The json data object
     * @param location The name of the wanted field
     * @param fallbackValue The value to return if the wanted field does not exist
     * @return T The value of the json field in the requested type, or fallbackValue if it does not exist
     * @throws std::exception If there is an error parsing the optional field
     */
    template <typename T>
    T getOptionalJsonField(const nlohmann::json& data_json, const std::string& location, const T& fallbackValue) noexcept {
        T val;
        try {
            val = getJsonField<T>(data_json, location);
        } catch (...) {
            return fallbackValue;
        }

        return val;
    }
} // namespace SimpleGFX::json
