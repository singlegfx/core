#pragma once

#include "logging/logger.hpp"
#include "simplegfx_helper.hpp"

#include "containers/asyncValueContainer.hpp"
#include "stdAddons/trackedFuture.hpp"

namespace SimpleGFX {

    /**
     * @brief Enum representing the action that triggered an input event.
     *
     * This enum is used to represent the different actions that can trigger an input event,
     * such as when a button is pressed or released. Each value corresponds to a specific
     * type of input action, and is used to provide additional information about the input
     * event.
     */
    enum inputAction {
        /**
         * @brief The button was pressed down.
         *
         * This value represents a button press event. It is used to indicate that a button
         * has been pressed down, and can be used to trigger specific actions in response to
         * the press event.
         */
        press,

        /**
         * @brief The button was released.
         *
         * This value represents a button release event. It is used to indicate that a button
         * has been released, and can be used to trigger specific actions in response to the
         * release event.
         */
        release,

        /**
         * @brief The button is being held down.
         *
         * This value represents a button hold event. It is used to indicate that a button is
         * currently being held down, and can be used to trigger specific actions in response
         * to the hold event.
         */
        hold,

        /**
         * @brief The input value was updated.
         *
         * This value represents an update event. It is used to indicate that the input value
         * has been updated or refreshed, and can be used to trigger specific actions in
         * response to the update event. It is the mose generic type and the default for
         * an inputEvent.
         */
        update
    };

    /**
     * @brief A class representing an input event.
     *
     * This class is used to represent an input event, such as when a button is pressed or a
     * joystick is moved. It contains information about the type of input, as well as any
     * additional data that may be associated with the input (such as the amount of force
     * applied to a button).
     */
    class inputEvent {
      public:
        /**
         * @brief The name or ID of this input event.
         *
         * This string contains a unique identifier for the input event, which can be used to
         * distinguish between different types of input events. For example, different buttons
         * on a game controller might have different names or IDs that can be used to identify
         * them.
         */
        std::string name = "";

        /**
         * @brief The name of the origin of this input event.
         *
         * This can be used by eventHandles to detect which eventPoller triggered the event
         * making it possible to handle events from different sources differently. This also
         * shortens the name of the inputEvent making it easier to read and parse.
         */
        std::string originName = "eventPoller";

        /**
         * @brief The amount of input associated with this event.
         *
         * This value represents the amount of input associated with the event, such as the
         * amount of force applied to a button or the angle of a joystick. It is typically a
         * double-precision floating-point value, although it may be represented using other
         * data types depending on the specific application.
         */
        double amount = 0;

        /**
         * @brief The type of input action that triggered this event.
         *
         * This value represents the type of input action that triggered the event, such as a
         * button press or release. It is an instance of the inputAction enum, and is used to
         * provide additional information about the input event.
         */
        inputAction inputType = update;
    };

    class eventManager;

    /**
     * @brief the functor type for handling input events.
     *
     * @param event The input event to handle.
     * @param handled A boolean value indicating whether the event has already been handled.
     * @return void
     */
    using eventFunctorType = sec_sigc::func_obj<void, const inputEvent&, bool&>;

    /**
     * @brief the functor type for polling input events.
     *
     * @param manager The event manager which emitted the signal (this). This is were the eventPoller should raise events.
     */
    using pollFunctorType = sec_sigc::func_obj<void, eventManager&>;

    /**
     * @brief An abstract class that represents an event handler.
     *
     * This class provides an interface to handle incoming input events. It is
     * meant to be inherited by a custom class that implements its own version of
     * the onEvent() function to actually handle events.
     *
     * @note Make sure you implement the operator()(const inputEvent&, bool&) function in your class.
     *
     * @warning only directly inherit from this class if your class also inherits from sec_trackable.
     */
    class [[maybe_unused]] eventHandle : virtual public eventFunctorType {
      protected:
        eventHandle() = default;

      public:
        ~eventHandle() = default;

        /**
         * @brief Called when an input event is raised. This function should be overridden
         * by the implementing class to handle the event. By default, this function does
         * just return the handled parameter.
         *
         * @param event - The input event object to handle.
         * @param handled - A boolean value indicating whether the event has already been handled.
         * @return bool - Returns true if the event was handled, false otherwise.
         */
        //void operator()(const inputEvent& event, bool& handled) = 0;
    };

    /**
     * @brief The tracked version of the eventHandle class.
     * This is the class you should inherit from if you want to create a new eventHandle.
     * It sets up the class to be trackable and the handling interface.
     * @note just like with the eventHandle class you need to implement the operator()(const inputEvent&, bool&) function in your class.
     */
    class [[maybe_unused]] tracked_eventHandle : virtual public eventHandle, public sec_sigc::sec_trackable {
        protected:
        tracked_eventHandle() = default;
      public:
        ~tracked_eventHandle() = default;
    };

    /**
     * @brief An abstract class that represents an event poller.
     *
     * This class provides an interface to detect if new input events should be raised. It
     * is meant to be inherited by a custom class that implements its own version of the
     * poll() function to poll for new events.
     *
     * @note
     * Make sure you implement the operator()(eventManager&) function in your class.
     *
     * @warning only directly inherit from this class if your class also inherits from sec_trackable.
     * In all other cases use the tracked_eventPoller class.
     */
    class [[maybe_unused]] eventPoller : virtual public pollFunctorType {
      protected:
        eventPoller() = default;

      public:
        virtual ~eventPoller() = default;

        /**
         * @brief Starts polling for new input events. This function should be overridden by
         * the implementing class to actually poll for events.
         */
        //void operator()(eventManager& manager) = 0;
    };

    /**
     * @brief The tracked version of the eventPoller class.
     * This is the class you should inherit from if you want to create a new eventPoller.
     * It sets up the class to be trackable and the polling interface.
     * @note just like with the eventPoller class you need to implement the operator()(eventManager&) function in your class.
     */
    class [[maybe_unused]] tracked_eventPoller : virtual public eventPoller, public sec_sigc::sec_trackable {
      protected:
        tracked_eventPoller()           = default;
      public:
        ~tracked_eventPoller() override = default;
    };

    /**
     * @brief Represents an event manager that can register and handle events through event handlers and event pollers.
     *
     * The event manager maintains a list of registered event handlers and event pollers. Event handlers are objects that
     * can respond to specific input events, while event pollers are objects that can periodically check for new events
     * and raise them to be handled by event handlers. The event manager provides methods for registering event handlers
     * and event pollers, as well as raising new events to be handled by all registered event handlers.
     */
    class [[maybe_unused]] eventManager : sec_sigc::sec_trackable {
      private:
        /**
         * @brief Vector of unhandled events along with their promises.
         *
         * This vector stores pairs of an input event and a shared_ptr to a tracked promise that resolves to true when the event
         * has been handled by all event handlers.
         */
        std::vector<std::pair<inputEvent, std::shared_ptr<trackedPromise<bool>>>> unhandledEvents;

        /**
         * @brief A mutex to secure the unhandledEvents vector for multithreading.
         */
        std::shared_mutex eventMutex;

        /**
         * @brief Future of the event handling thread.
         */
        std::future<void> eventThread;

        /**
         * @brief The loggingInterface used to log information
         */
        std::shared_ptr<logger> loggingInterface = nullptr;

        /**
         * @brief A boolean flag that tracks whether this event poller is in the process of being
         * destroyed. When this flag is true, no further events should be polled or raised. In addition to that
         * no new eventHandle are eventPoller should be added.
         */
        std::atomic<bool> quitting = false;

        /**
         * @brief The signal all event pollers are connected to.
         * @param eventManager The event manager which emitted the signal (this). This is were the eventPoller should raise events.
         */
        sec_sigc::sec_signal<void, eventManager&> eventPollSignal;

        /**
         * @brief The signal all event handlers are connected to.
         * @param event The event to handle.
         * @return bool True if the event was handled, false otherwise.
         */
        sec_sigc::sec_signal<void, const inputEvent&, bool&> eventHandleSignal;

      public:
        /**
         * @brief Constructs a new event manager object.
         */
        explicit eventManager(std::shared_ptr<logger> loggingInterface = nullptr);

        /**
         * @brief Destroys the event manager object.
         */
        ~eventManager();

        /**
         * @brief Raises a new event that should be handled by all event handlers.
         *
         * @param event The event to raise.
         * @return std::shared_ptr<trackedFuture<bool>> Shared pointer to a tracked future that resolves to true when the event has
         * been handled by all event handlers.
         */
        [[maybe_unused]]
        std::shared_ptr<trackedFuture<bool>> raiseEvent(const inputEvent& event);

        [[maybe_unused]]
        std::vector<std::shared_ptr<trackedFuture<bool>>> raiseEvents(const std::vector<inputEvent>& events);

        /**
         * @brief Manually polls for new events.
         */
        [[maybe_unused]]
        void poll();

        /**
         * @brief Registers a new event handler.
         * @warning make sure this functor is trackable, a function pointer or a lambda expression.
         * Otherwise a call after free might happen.
         * @param handler The event handler to register.
         */
        template <class eventClass>
            requires std::derived_from<eventClass, eventHandle> && std::derived_from<eventClass, sec_sigc::sec_trackable>
        [[maybe_unused]]
        void registerHandler(eventClass& handler) {
            if (quitting) {
                throw std::runtime_error("cannot register handle while quitting");
            }

            eventHandleSignal.connect(handler);
        }

        /**
         * @brief Registers a new event poller.
         * @warning make sure this functor is trackable, a function pointer or a lambda expression.
         * Otherwise a call after free might happen.
         * @param poller The event poller to register.
         */
        template <class pollClass>
            requires std::derived_from<pollClass, eventPoller> && std::derived_from<pollClass, sec_sigc::sec_trackable>
        [[maybe_unused]]
        void registerPoller(pollClass& poller) {
            if (quitting) {
                throw std::runtime_error("cannot register poller while quitting");
            }

            eventPollSignal.connect(poller);
        }
    };

} // namespace SimpleGFX
