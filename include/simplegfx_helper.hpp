#pragma once

#include "stdAddons/chronoUtils.hpp"
#include "stdAddons/containerUtils.hpp"
#include "stdAddons/exceptionUtils.hpp"
#include "stdAddons/filesystemUtils.hpp"
#include "stdAddons/numberUtils.hpp"
#include "stdAddons/stringUtils.hpp"
#include "stdAddons/typedSwitch.hpp"
#include "json/jsonUtils.hpp"

namespace SimpleGFX {

    /**
     * @brief A helper class that provides various functions for functionality that is needed across different classes and components.
     *
     * This class includes functions for printing exceptions with proper indentation, splitting a string into substrings using a delimiter
     * character, retrieving specific fields from JSON data objects safely, getting the current time point using the high-resolution clock,
     * enabling switch statements with any data type, loading file contents into a string, and checking if an element is present in a
     * vector. Additionally, this class provides template functions for comparing whether two values are equal within a specified tolerance,
     * implementing a switch statement specifically for strings, searching an unsorted vector for a given value, inserting the contents of a
     * second vector into the end of a destination vector, and appending a single value to an existing std::array.
     *
     */
    class SIMPLEGFX_EXPORT_MACRO [[deprecated("Use the functions directly instead of using this class")]] helper {
      public:
        [[deprecated("Use SimpleGFX::exception::decodeException directly")]]
        static void decodeException(const std::exception& e, std::stringstream& outputStream, int level = 0) {
            exception::decodeException(e, outputStream, level);
        }

        [[deprecated("Use SimpleGFX::exception::printException directly")]]
        static void printException(const std::exception& e) {
            exception::printException(e);
        }

        [[deprecated("Use SimpleGFX::chrono::now directly")]] [[nodiscard]]
        static std::chrono::time_point<std::chrono::high_resolution_clock> now() {
            return chrono::now();
        }

        template <typename T>
        [[nodiscard]] [[deprecated("Use SimpleGFX::numbers::isRoughly directly")]]
        static bool isRoughly(T value1, T value2, double precision = 0.001) {
            return numbers::isRoughly(value1, value2, precision);
        }

        template <typename T>
        [[nodiscard]] [[deprecated("Use SimpleGFX::TSwitch directly")]]
        static int64_t TSwitch(const T& value, const std::vector<T>& cases) {
            return ::SimpleGFX::TSwitch<T>(value, cases);
        }

        [[nodiscard]] [[deprecated("Use SimpleGFX::TSwitch<std::string> directly")]]
        static int64_t stringSwitch(const std::string& value, const std::vector<std::string>& cases) {
            return ::SimpleGFX::TSwitch<std::string>(value, cases);
        }

        template <typename T, std::ranges::range R>
            requires std::equality_comparable_with<std::ranges::range_value_t<R>, T>
        [[deprecated("Use std::ranges::contains directly")]] [[nodiscard]]
        static bool contains(const R& data, const T& key) {
            return std::ranges::contains(data, key);
        }

        template <typename T>
        [[deprecated("Use std::ranges::find directly")]] [[nodiscard]]
        static decltype(auto) searchArray(const std::vector<T>& data, const T& key) {
            return std::ranges::find(data, key);
        }

        template <typename T>
        [[deprecated("Use std::vector::appendRange directly")]]
        static void emplaceBack(std::vector<T>& destination, const std::vector<T>& source) {
            return destination.appendRange(source);
        }

        template <typename T, size_t SIZE>
        [[deprecated("Use SimpleGFX::container::appendValue directly")]]
        static void appendValue(std::array<T, SIZE>& data, const T& value) {
            container::appendValue<T, std::array<T, SIZE>>(data, value);
        }

        [[nodiscard]] [[deprecated("Use SimpleGFX::filesystem::loadFile directly")]]
        static std::string loadFile(std::filesystem::path location) {
            return filesystem::loadFile(std::move(location));
        }

        [[nodiscard]] [[deprecated("Use SimpleGFX::filesystem::getUserDirectory directly")]]
        static std::filesystem::path getUserDirectory() {
            return filesystem::getUserDirectory();
        }

        [[nodiscard]] [[deprecated("Use SimpleGFX::filesystem::getApplicationDirectory directly")]]
        static std::filesystem::path getApplicationDirectory(const std::string& appName) {
            return filesystem::getApplicationDirectory(appName);
        }

        [[deprecated("Use SimpleGFX::string::splitString directly")]]
        static std::vector<std::string> splitString(const std::string& s, char delimiter) {
            return SimpleGFX::string::splitString(s, delimiter);
        }

        [[deprecated("Use SimpleGFX::string::removeWhitespace directly")]]
        static std::string removeWhitespace(const std::string& s) {
            return SimpleGFX::string::removeWhitespace(s);
        }

        [[deprecated("Use SimpleGFX::string::toLower directly")]]
        static std::string toLower(const std::string& s) {
            return SimpleGFX::string::toLower(s);
        }

        [[deprecated("Use SimpleGFX::json::getJsonField directly")]]
        static nlohmann::json getJsonField(const nlohmann::json& data_json, const std::string& location) {
            return SimpleGFX::json::getJsonField(data_json, location);
        }

        [[deprecated("Use SimpleGFX::json::getOptionalJsonField directly")]]
        static std::optional<nlohmann::json> getOptionalJsonField(const nlohmann::json& data_json, const std::string& location) {
            return SimpleGFX::json::getOptionalJsonField(data_json, location);
        }

        template <typename T>
        [[deprecated("Use SimpleGFX::json::getJsonField directly")]]
        static T getJsonField(const nlohmann::json& data_json, const std::string& location) {
            return SimpleGFX::json::getJsonField<T>(data_json, location);
        }

        template <typename T>
        [[deprecated("Use SimpleGFX::json::getOptionalJsonField directly")]]
        static std::optional<T> getOptionalJsonField(const nlohmann::json& data_json, const std::string& location) {
            return SimpleGFX::json::getOptionalJsonField<T>(data_json, location);
        }

        template <typename T>
        [[deprecated("Use SimpleGFX::json::getOptionalJsonField directly")]]
        static T getOptionalJsonField(const nlohmann::json& data_json, const std::string& location, const T& fallbackValue) {
            return SimpleGFX::json::getOptionalJsonField<T>(data_json, location, fallbackValue);
        }
    };

} // namespace SimpleGFX
