#pragma once

#include <string>
#include <vector>

#ifndef SIMPLEGFX_EXPORT_MACRO
    #define SIMPLEGFX_EXPORT_MACRO
#endif

namespace SimpleGFX::string {

    /**
     * @brief Splits a given string into several substrings using a specified
     *        delimiter character.
     *
     * This function takes a single string and a single delimiter character and
     * returns a vector of substrings created by splitting the original string at
     * each occurrence of the delimiter character. The delimiter character itself is
     * not included in any of the substrings.
     *
     * @param s The string to be split.
     * @param delimiter The character to be used as the delimiter.
     *
     * @return A vector containing the substrings created by splitting the original
     *         string.
     *
     * @note This implementation uses the standard istringstream class to parse
     *       the input string, which may introduce some overhead for larger strings.
     */
    SIMPLEGFX_EXPORT_MACRO std::vector<std::string> splitString(const std::string& s, char delimiter);

    /**
     * @brief Removes all whitespace characters from a given string.
     *
     * This function takes a single string and returns a copy of that string with
     * all whitespace characters removed. Whitespace characters include spaces,
     * tabs, newlines, carriage returns, and form feeds.
     *
     * @param s The string from which to remove whitespace characters.
     *
     * @return A copy of the input string with all whitespace characters removed.
     */
    SIMPLEGFX_EXPORT_MACRO std::string removeWhitespace(const std::string& s);

    /**
     * @brief Converts a given string to lowercase.
     *
     * This function takes a single string and returns a copy of that string with
     * all uppercase characters converted to lowercase.
     *
     * @param s The string to be converted to lowercase.
     *
     * @return A copy of the input string with all uppercase characters converted
     *         to lowercase.
     */
    SIMPLEGFX_EXPORT_MACRO std::string toLower(const std::string& s);

} // namespace SimpleGFX::string