#pragma once

#include "concepts.hpp"

#include <chrono>

namespace SimpleGFX::chrono {
    /**
     * @brief Returns the current time point using the high resolution clock
     *
     * This function returns the current time point using the high resolution
     * clock which provides time measurements with high accuracy and precision.
     *
     * @return std::chrono::time_point<std::chrono::high_resolution_clock>
     * The current time point
     */
    SIMPLEGFX_EXPORT_MACRO std::chrono::time_point<std::chrono::high_resolution_clock> now();
} // namespace SimpleGFX::chrono
