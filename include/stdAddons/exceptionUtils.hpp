#pragma once

#include "concepts.hpp"

#include <exception>

namespace SimpleGFX::exception {
    /**
     * @brief Prints a given std::exception to the outputStream, including any nested exception(s), with proper indentation
     */
    SIMPLEGFX_EXPORT_MACRO void decodeException(const std::exception& e, std::stringstream& outputStream, int level = 0);

    /**
     * @brief Prints a given std::exception, including any nested exception(s), with proper indentation
     *
     * This function adds an indentation for every level of nested exception.
     *
     * @param e The (nested) exception to print
     */
    SIMPLEGFX_EXPORT_MACRO void printException(const std::exception& e);
} // namespace SimpleGFX::exception