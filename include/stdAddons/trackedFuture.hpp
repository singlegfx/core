#pragma once

#include <chrono>
#include <future>
#include <memory>
#include <thread>

namespace SimpleGFX {

    // Forward declaration of trackedPromise, so it can be used in trackedFuture.
    template <typename T>
    class trackedPromise;

    /**
     * @brief A future class that provides additional functionality in comparison with std::future,
     * and is designed to be used with a corresponding promise object (`trackedPromise`).
     *
     * @tparam T A template parameter.
     */
    template <typename T>
    class trackedFuture {
        friend class trackedPromise<T>;

      private:
        /**
         * @brief A reference to the promise object that is used for synchronization between the producer
         * (promise) and the consumer (future) of the value.
         */
        trackedPromise<T>& internalPromise;

        /**
         * @brief Constructs a trackedFuture object with a reference to the promise object used for synchronization
         * between the producer (promise) and the consumer (future) of the value.
         *
         * @param prom A reference to the trackedPromise object.
         */
        explicit trackedFuture(trackedPromise<T>& prom);

        /**
         * @brief An atomic boolean that tracks the validity of the future object, i.e., whether it was
         * properly constructed and not yet invalidated.
         */
        std::atomic<bool> isValid = true;

      public:
        /**
         * @brief Prevent copying, moving or default-constructing the object by making these constructors
         * private and not implemented.
         */
        trackedFuture(trackedFuture<T>& prom) = delete;
        trackedFuture(const trackedFuture<T>& prom) = delete;
        trackedFuture(trackedFuture<T>&& prom) = delete;
        trackedFuture() = delete;

        /**
         * @brief A destructor that releases any resources used by the object and invalidates the
         * corresponding future object if needed.
         */
        ~trackedFuture();

        /**
         * @brief Use this function to indicate that the consumer of the value doesn't need it anymore
         * and won't attempt to retrieve it. This allows the promise object to release any held resources
         * as soon as possible.
         */
        void drop();

        /**
         * @brief Return true if the future object is valid, otherwise false.
         */
        bool valid();

        /**
         * @brief Block the calling thread until the value is set by the promise object and associated
         * with this future.
         */
        void wait();

        /**
         * @brief Retrieve the value that was set by the promise object and associated with this future.
         * This function blocks until the value is available, and returns a copy of it.
         *
         * @warning This function may throw an exception of the `std::future_error` type if the future
         * object is not in a valid state (e.g., invalidated or moved).
         */
        T get();
    };

    /**
     * @brief A more complex version of std::promise that works together with trackedFuture.
     *
     * @tparam T A template parameter.
     */
    template <typename T>
    class trackedPromise {
        friend class trackedFuture<T>;

      private:
        /**
         * @brief The value of the promise.
         */
        T value;

        /**
         * @brief Shared pointer to the internal future object.
         */
        std::shared_ptr<trackedFuture<T>> internalFuture = nullptr;

        /**
         * @brief Atomic booleans for tracking whether the value has been read.
         */
        std::atomic<bool> readValue = false;

        /**
         * @brief Atomic booleans for tracking whether the value has been set.
         */
        std::atomic<bool> setValue = false;

        /**
         * @brief Constructor. Creates a new trackedPromise object.
         */
        trackedPromise() {
            readValue.store(false);
            setValue.store(false);
        };

      public:
        /**
         * @brief Creates a new trackedPromise object.
         */
        static std::shared_ptr<trackedPromise<T>> create() { return std::shared_ptr<trackedPromise<T>>(new trackedPromise<T>{}); };

        /**
         * @brief Destructor. Ensures proper destruction of the associated trackedFuture object.
         */
        ~trackedPromise();

        /**
         * @brief Disable copy/move constructors and default constructor.
         */
        trackedPromise(trackedPromise<T>& other) = delete;
        trackedPromise(const trackedPromise<T>& other) = delete;
        trackedPromise(trackedPromise<T>&& other) = delete;

        /**
         * @brief Returns a shared pointer to the associated trackedFuture object, which can be used to
         * retrieve the set value.
         *
         * @return std::shared_ptr<trackedFuture<T>> A shared pointer to the trackedFuture object.
         */
        std::shared_ptr<trackedFuture<T>> get_future();

        /**
         * @brief Sets the value of the promise, which can be retrieved by any associated trackedFuture
         * objects.
         *
         * @param val A parameter of type const T& representing the value to be set.
         */
        void set_value(const T& val);

        /**
         * @brief Returns true if the value of the promise has been read by associated trackedFuture object(s),
         * otherwise false.
         *
         * @return bool True if the value has been read, otherwise false.
         */
        bool is_read();

        /**
         * @brief Returns true if the value of the promise has been set, otherwise false.
         *
         * @return bool True if the value has been set, otherwise false.
         */
        bool is_set();
    };

    template <typename T>
    trackedPromise<T>::~trackedPromise() {
        internalFuture->isValid.store(false);
    }

    template <typename T>
    std::shared_ptr<trackedFuture<T>> trackedPromise<T>::get_future() {
        if (internalFuture == nullptr) {
            internalFuture = std::shared_ptr<trackedFuture<T>>(new trackedFuture<T>{*this});
        }

        return internalFuture;
    }

    template <typename T>
    void trackedPromise<T>::set_value(const T& val) {
        if (is_set()) {
            return;
        }
        value = val;
        setValue.store(true);
    }

    template <typename T>
    bool trackedPromise<T>::is_read() {
        return readValue.load();
    }

    template <typename T>
    bool trackedPromise<T>::is_set() {
        return setValue.load();
    }

    // implementation of trackedFuture
    template <typename T>
    trackedFuture<T>::trackedFuture(trackedPromise<T>& prom)
        : internalPromise{prom} {}
    template <typename T>
    trackedFuture<T>::~trackedFuture() {}

    template <typename T>
    void trackedFuture<T>::drop() {
        if (valid()) {
            internalPromise.readValue = true;
        }
    }

    template <typename T>
    bool trackedFuture<T>::valid() {
        return isValid.load();
    }

    template <typename T>
    T trackedFuture<T>::get() {
        if (!isValid) {
            throw std::future_error{std::future_errc::no_state};
        }
        wait();
        auto retVal = internalPromise.value;
        internalPromise.readValue.store(true);
        return retVal;
    }

    template <typename T>
    void trackedFuture<T>::wait() {
        if (!valid()) {
            throw std::future_error{std::future_errc::no_state};
        }

        while (!internalPromise.setValue) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));

            if (!valid()) {
                throw std::future_error{std::future_errc::no_state};
            }
        }
    }

} // namespace SimpleGFX
