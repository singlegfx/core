#pragma once

#include "concepts.hpp"

#include <cstdint>
#include <functional>
#include <vector>

namespace SimpleGFX {
    /**
     * @brief Compares two values and returns true if they are equal.
     * This function requires the two types to be equality comparable.
     * This just calls the == operator on the two values.
     * @tparam T1 The type of the first value.
     * @tparam T2 The type of the second value.
     * @param a The first value.
     * @param b The second value.
     * @return true if the two values are equal, otherwise false.
     */
    template <typename T1, typename T2>
        requires std::equality_comparable_with<T1, T2>
    bool simpleCompare(const T1& a, const T2& b) {
        return a == b;
    }

    /**
     * @brief Enables switch statements with any data type.
     *
     * Given a value to tests and a vector containing all possible cases, this function returns the index of the first matched case. It
     * allows developers to use any data type as a parameter in a switch statement.
     *
     * @par How to use
     *
     *  To use this function, first pass in the variable that is going to be tested in the switch statement, followed by a vector
     * containing all of the possible cases. The function returns the index of the first matched case or -1 if no match is found. Here's
     * an example:
     *
     *    ```cpp
     *       std::string myString = "b";
     *       std::vector<std::string> possibleCases = {"a", "b", "c"};
     *
     *       switch(helper::TSwitch<std::string>(myString, possibleCases)){
     *           case(0):
     *               std::cout << "myString is a" << std::endl;
     *               break;
     *           case(1):
     *               std::cout << "myString is b" << std::endl;
     *               break;
     *           case(2):
     *               std::cout << "myString is c" << std::endl;
     *               break;
     *           default:
     *               std::cout << "myString is something else" << std::endl;
     *               break;
     *       }
     *    ```
     *
     * This function can also be used to obtain the index of a particular item in an array.
     *
     * @param value  The variable that is going to be tested in the switch statement.
     * @param cases  A range containing all possible cases in order.
     * @param compareFunc A function that compares two values and returns true if they are equal.
     *
     * @warning automatic template deduction seems to fail when a compare function is passed.
     * In that case you need to explicitly specify the template parameters.
     * You can write a wrapper function that calls this function with the correct template parameters.
     * Below is an example on how such a function can look like when using the isRoughly function from SimpleGFX:
     * ```cpp
     *   template <typename T, typename C>
     *   static int64_t roughlyTSwitch(const T& value, const C& cases) {
     *      return TSwitch<T, C>(value, cases, [](const auto& a, const auto& b){return SimpleGFX::numbers::isRoughly(a, b);});
     *   }
     * ```
     * As you can see the call to isRoughly is wrapped in a lambda function.
     * Without it the type deduction fails for some reason even if the template parameters are explicitly specified.
     *
     * @return The index of the first matched case or -1 if no match is found.
     */
    template <typename T, std::ranges::range R>
        requires std::convertible_to<std::ranges::range_value_t<R>, T>
    int64_t TSwitch(const T& value, const R& cases, std::function<bool(const T&, const T&)> compareFunc = simpleCompare<T, T>) {
        auto current = std::ranges::begin(cases);
        auto end   = std::ranges::end(cases);

        for (int64_t i = 0; current != end; current++, i++) {
            if (compareFunc(value, *current)) {
                return i;
            }
        }

        return -1;
    }
} // namespace SimpleGFX