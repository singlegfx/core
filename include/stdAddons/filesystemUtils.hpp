#pragma once

#include "concepts.hpp"

#include <filesystem>

/**
 * This namespace contains all the functions that are used to interact with the filesystem.
 * Most of them extend the functionality of the std::filesystem namespace.
 */
namespace SimpleGFX::filesystem {
    /**
     * @brief Loads a file into a string
     *
     * This function provides a simple interface for loading the contents of a file
     * into a string. It takes a file location as a parameter and opens a file stream
     * to read the contents of the file. The file contents are then stored in a string
     * and returned by the function.
     *
     * @param location The file location to read from as std::filesystem::path
     * @return The data from the file as a string
     *
     * @throws std::nested_exception This function may throw a nested exception if
     * there are any issues when reading the file. The error message will provide
     * additional details about the issue encountered.
     *
     * @note The function supports files that can be read using text streams.
     * It is not designed for use with binary files.
     *
     * Example usage:
     * @code
     * std::filesystem::path filepath = "path/to/my/file.txt";
     * auto fileData = loadFile(filepath);
     * @endcode
     */
    [[nodiscard]]
    SIMPLEGFX_EXPORT_MACRO std::string loadFile(std::filesystem::path location);

    /**
     * @brief Construct an object from a loaded file
     *
     * This function provides a simple interface for constructing an object from a loaded file.
     * It uses the loadFile function to load the file and then constructs the object from the loaded data.
     * If needed constructor arguments can be passed to the function and they will be forwarded to the constructor.
     *
     * @warning for this to work the first argument of the constructor must be a std::string
     *
     * @tparam T the type of the object to construct
     * @tparam args the types of the constructor arguments
     * @param location the location of the file to load
     * @param constructorArgs the constructor arguments
     * @return the constructed object
     */
    template <class T, typename... args>
    [[nodiscard]]
    T&& constructFromLoadedFile(std::filesystem::path location, args... constructorArgs) {
        try {
            auto data = loadFile(location);
            return T{std::move(data), &constructorArgs...};
        } catch (...) {
            std::throw_with_nested(std::runtime_error("Failed to construct object from file: " + location.string()));
        }
    }

    /**
     * @brief get the user directory of the current user
     * @details This function returns the path to the user directory of the current user across all desktop operating systems.
     *
     * * On Windows, the path will be something like C:\Users\username.
     * * On macOS, the path will be something like /Users/username.
     * * On Linux, the path will be something like /home/username.
     *
     * @throws std::runtime_error if the user directory could not be found
     * @return the path to the user directory
     */
    [[nodiscard]]
    SIMPLEGFX_EXPORT_MACRO std::filesystem::path getUserDirectory();

    /**
     * @brief get the user local folder for the given application
     * @details This function returns the path to the user local folder for the given application across all desktop operating systems.
     * The app name should be the name of the application as it appears in the application manifest.
     * If GTK is used the the Gio::Application::id_is_valid() function can be used to check if the app name is valid.
     * The name passed to this should be the id that is used to create the application.
     *
     * If you somehow have no reference to the application object, you can use the Gio::Application::get_default()->get_id() to get the
     * id of the default application. Note that this will only work if the application has already been created and registered.
     *
     * * On Windows, the path will be something like C:\Users\username\AppData\Local\appName.
     * * On macOS, the path will be something like /Users/username/Library/Application Support/appName.
     * * On Linux, the path will be something like /home/username/.local/share/appName.
     *
     * @throws std::runtime_error if the user directory could not be found
     * @param appName the name of the application
     * @return the path to the user local folder for the given application
     */
    [[nodiscard]]
    SIMPLEGFX_EXPORT_MACRO std::filesystem::path getApplicationDirectory(const std::string& appName);
} // namespace SimpleGFX::filesystem
