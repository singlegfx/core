#pragma once

#include "concepts.hpp"

namespace SimpleGFX::container {

    /**
     * @brief Append a single value to an existing std::array.
     *
     * This function moves all the existing elements in the array by one position to the front and then adds the new element to the back
     * of the array.
     *
     * @tparam T The type of the value and the array. (must be copy constructible and movable)
     * @tparam SIZE The number of elements this array has. (must be greater than zero)
     * @param data The std::array reference to which the value should be appended.
     * @param value The value to be appended to the array.
     *
     * @note This function assumes that the std::array has at least one element.
     *
     * @warning Attempting to append a value to an std::array with SIZE equal to zero
     * will result in the type requirement notZeroSize being violated and a compile-time error.
     */
#ifndef SIMPLEGFX_SIZED_RANGE_APPEND
    template <std::copy_constructible T, size_t SIZE>
        requires SimpleGFX::Concepts::notZeroSize<SIZE>
    void appendValue(std::array<T, SIZE>& data, const T& value) {
        for (size_t i = 0; i < SIZE - 1; i++) {
            data[i] = std::move(data[i + 1]);
        }

        // Add the new element to the back of the std::array.
        data[SIZE - 1] = value;
    }

#else
    /*
     * This is the same function as above but allows for all sized ranges.
     * I can't get it the size requirement to work though so it is commented out for now.
     *
     */
    template <std::copy_constructible T, std::ranges::sized_range R>
        requires std::same_as<std::ranges::range_value_t<R>, T> && requires(R& r) { std::ranges::size(r) > 0; }
    void appendValue(R& data, const T& value) {
        auto begin = std::ranges::begin(data);
        auto end   = std::ranges::end(data);

        while (begin + 1 != end) {
            *begin = std::move(*(begin + 1));
            begin++;
        }

        *begin = value;
    }
#endif

} // namespace SimpleGFX::container