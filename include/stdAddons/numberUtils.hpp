#pragma once

#include "concepts.hpp"

#include <algorithm>

namespace SimpleGFX::numbers {

    /**
     * @brief Returns the absolute value of a numeric type.
     * This function returns the absolute value of a numeric type.
     * Unlike the C function this does not rely on a comparison with 0.
     * Instead the inverted value is compared with the original value.
     * @note This function requires the type to be invertable and comparable.
     * @tparam T The type of the value.
     * @param value The value to get the absolute value of.
     * @return The absolute value of the input.
     */
    template <typename T>
        requires std::three_way_comparable<T> && Concepts::invertable<T>
    T absT(T value) {
        auto inv = -value;
        return value > inv ? value : inv;
    }

    /**
     * @brief Compares whether two values are equal with a specified tolerance.
     *
     * This function checks if two values of any data type are equal within a certain degree of tolerance.
     * This is useful for comparing floating point values, where small variations in precision can result in different values.
     * If the values are equal, the function will return true, otherwise it will return false.
     *
     * @tparam T The data type of the values being compared. It can be any type that supports the equality operator (==).
     * @param value1 The first value being compared.
     * @param value2 The second value being compared.
     * @param precision The level of tolerance allowed between the two values.
     *        The default is 0.001, which should be fine for most cases.
     *        If you need more or less precision, you can adjust this value accordingly.
     * @return True if the two values are essentially equal within the specified tolerance, otherwise False.
     *
     * @note
     * - If the two values are exactly equal (i.e. value1 == value2), the function will return true.
     * - This function uses std::abs to calculate the absolute values of the input, so the template parameter T must
     *   be a type that supports the std::abs function.
     * - The precision value should be chosen carefully, as choosing too small of a value may cause the function
     *   to incorrectly return false negatives, while choosing too large of a value may increase the likelihood
     *   of the function returning false positives.
     */
    template <typename T1, typename T2>
        requires std::three_way_comparable_with<T1, T2> && Concepts::invertable<T1> && Concepts::invertable<T2> &&
                 requires(T1 a, T2 b, double p) {
                     { a* p } -> std::convertible_to<T1>;
                     { b* p } -> std::convertible_to<T2>;
                     { a == b } -> std::convertible_to<bool>;
                     { a - b } -> std::convertible_to<T1>;
                     { b - a } -> std::convertible_to<T2>;
                 }
    bool isRoughly(T1 value1, T2 value2, double precision = 0.001) {
        // check if the values are exactly equal
        // this is needed in case both values are 0
        if (value1 == value2) {
            return true;
        }

        // create the absolute values of the two values and compare them
        // these will be used more than once so storing them in variables makes it more readable
        auto absV1   = absT(value1);
        auto absV2   = absT(value2);
        auto absComp = absV1 <=> absV2;

        // split the code into two branches
        // this is needed since the type of the values may be different
        // splitting it into two branches guarantees that all types are static at compile time
        if (absComp > 0) {
            // get the epsilon by multiplying the larger value by the precision
            auto epsilon = static_cast<T1>(absV1 * absT(precision));

            // if the absolute value of the difference between the two values is less than or equal to the epsilon, return true
            return absT(static_cast<T2>(value1 - value2)) <= epsilon;
        } else {
            // get the epsilon by multiplying the larger value by the precision
            auto epsilon = static_cast<T2>(absV2 * absT(precision));

            // if the absolute value of the difference between the two values is less than or equal to the epsilon, return true
            return absT(static_cast<T2>(value2 - value1)) <= epsilon;
        }
    }
} // namespace SimpleGFX::numbers
