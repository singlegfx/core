#pragma once

#include <concepts>
#include <ranges>

#include <sec_sigc++.hpp>

#ifndef SIMPLEGFX_EXPORT_MACRO
    #define SIMPLEGFX_EXPORT_MACRO
#endif

namespace SimpleGFX::Concepts {

    /**
     * @brief This concept can be used to easily set a requirement not 0 requirement for size_t template parameters.
     *
     * @note you cannot use this template instead of size_t as it is not a type.
     *
     * @detail The following code is an example on how to use this concept:
     * @code
     * template <size_t SIZE>
     *    requires SimpleGFX::Concepts::notZeroSize<SIZE>
     * void foo(std::array<int, SIZE>& data){
     *  // do something with the data
     *  // the compiler guarantees that the data has at least one value
     * }
     *
     * @tparam SIZE The size_t value to check.
     */
    template <size_t SIZE>
    concept notZeroSize = (SIZE > 0);

    /**
     * @brief This concept can be used to easily set a requirement for a type to be invertable.
     * This concept guarantees that the type has a unary minus operator and the result of this operator is of the same type.
     *
     * @tparam T the type to check (it does not have to be numeric but makes more sense in that case)
     */
    template <typename T>
    concept invertable = requires(T a) {
        { -a } -> std::convertible_to<T>;
    };
} // namespace SimpleGFX::Concepts
