#pragma once

/**
 * @brief SimpleGFX is a C++ library designed to provide core interfaces needed in order to program graphical programs.
 *
 * @details The SimpleGFX namespace encapsulates all of the types and functions that are part of the SimpleGFX project, including both
 * the core components of the library and additional functionality provided by sub-libraries.
 *
 * @note One such sub-library is SimpleGL, which is built on top of OpenGL and GTKmm4 to provide advanced graphics features. SimpleGL
 * is located in a sub-namespace of SimpleGFX for easy access and organization.
 *
 * @section Overview
 *
 * SimpleGFX consists of the following components:
 * - @ref SimpleGFX::eventSystem "eventSystem.hpp": A collection of classes to trigger and handle events from all kinds of inputs.
 * - @ref SimpleGFX::helper "simplegfx_helper.hpp": A helper class with various functions to manipulate arrays and JSON data.
 * - @ref SimpleGFX::trackedFuture "trackedFuture.hpp": A promise and future class to provide more features in a similar way to std::promise
 * and std::future.
 *
 * @subsection Sub-modules
 * - @ref SimpleGFX::SimpleGL "SimpleGL": A sub-library built on top of OpenGL and GTKmm4 to provide advanced graphics features.
 *
 */
namespace SimpleGFX {};

#include "concepts.hpp"
#include "json/jsonUtils.hpp"

#include "simplegfx_helper.hpp"

#include "events/eventSystem.hpp"

#include "logging/logger.hpp"
#include "logging/loggerJson.hpp"
#include "logging/loggerTxt.hpp"

#include "stdAddons/trackedFuture.hpp"

#include "containers/asyncValueContainer.hpp"
#include "containers/asyncFuncContainer.hpp"
