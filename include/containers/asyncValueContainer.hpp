#pragma once

#include "stdAddons/trackedFuture.hpp"
#include <vector>

namespace SimpleGFX {

    /**
     * @brief A container that can be used to store values asynchronously.
     * It contains data queues for adding and removing values, and a vector for storing the values.
     * This allows to add objects while iterating over the container.
     * The only way to access the values is by using the forEach or forEachCancellable functions.
     * This way the integrity of the data is ensured.
     *
     * @note This class is thread-safe.
     * @note This class is only intended to store values. There is a separate class for storing functions.
     * @tparam T The type of the values to store.
     */
    template <typename T>
    class asyncValueContainer {
      private:
        /**
         * @brief The vector with the registered data, their priority and their ID.
         */
        std::vector<std::tuple<T, int, uint64_t>> values;

        /**
         * @brief The index of the next value to be added.
         */
        std::atomic<uint64_t> nextValueID = 0;

        /**
         * @brief The queue of values to be added.
         * Like the values vector, this is a tuple containing the value, the priority.
         * Instead of the ID it contains a promise of an ID.
         * Its value is set when the value is added to the values vector.
         */
        std::vector<std::tuple<T, int, std::shared_ptr<SimpleGFX::trackedPromise<uint64_t>>>> queuedAddValues;

        /**
         * @brief The queue of values to be removed.
         * It contains the ID of the value to be removed and a promise of a bool.
         * The bool will be set to true if the value was removed.
         * If the value was not found, the bool will be set to false.
         * The bool will be set when the value is removed from the values vector.
         */
        std::vector<std::tuple<uint64_t, std::shared_ptr<SimpleGFX::trackedPromise<bool>>>> queuedRemoveIDs;

        /**
         * @brief This is used to keep the add promises alive until they are read.
         */
        std::vector<std::shared_ptr<SimpleGFX::trackedPromise<uint64_t>>> unreadAddPromises;

        /**
         * @brief This is used to keep the remove promises alive until they are read.
         */
        std::vector<std::shared_ptr<SimpleGFX::trackedPromise<bool>>> unreadRemovePromises;

        // these are the mutexes for each vector
        std::shared_mutex valueMutex;
        std::shared_mutex queueAddMutex;
        std::shared_mutex queueRemoveMutex;
        std::shared_mutex promiseAddMutex;
        std::shared_mutex promiseRemoveMutex;

      protected:
        /**
         * @brief Add all queued values to the values vector.
         * If you want more fine grained control over the update process you can use this instead of the update function.
         * This function locks the queueAddMutex, promiseAddMutex and valueMutex.
         * @note This function is called by the update function. Don't call it during a forEach loop or you will get a deadlock.
         */
        void addValues() {
            std::scoped_lock lock{queueAddMutex, promiseAddMutex, valueMutex};
            for (auto queuedEntry = queuedAddValues.begin(); queuedEntry < queuedAddValues.end(); queuedEntry++) {
                auto [handle, priority, promise] = std::move(*queuedEntry);
                queuedEntry = queuedAddValues.erase(queuedEntry);
                unreadAddPromises.emplace_back(promise);

                auto valueIterator = values.begin();
                while(valueIterator < values.end()){
                    if (std::get<1>(*valueIterator) > priority){ break; }
                    valueIterator++;
                }

                if(valueIterator == values.end()){
                    values.emplace_back(handle, priority, nextValueID);
                }else{
                    values.emplace(valueIterator, handle, priority, nextValueID);
                }

                promise->set_value(nextValueID);
                nextValueID++;
            }
        }

        /**
         * @brief Remove all queued values from the values vector.
         * If you want more fine grained control over the update process you can use this instead of the update function.
         * This function locks the queueRemoveMutex, promiseRemoveMutex and valueMutex.
         * @note This function is called by the update function. Don't call it during a forEach loop or you will get a deadlock.
         */
        void removeValues() {
            std::scoped_lock lock{queueRemoveMutex, promiseRemoveMutex, valueMutex};
            for (auto queuedEntry = queuedRemoveIDs.begin(); queuedEntry < queuedRemoveIDs.end(); queuedEntry++) {
                auto [ID, promise] = std::move(*queuedEntry);
                queuedEntry = queuedRemoveIDs.erase(queuedEntry);
                unreadRemovePromises.emplace_back(promise);

                for (auto i = values.begin(); i < values.end(); i++) {
                    if (std::get<2>(*i) == ID) {
                        values.erase(i);
                        promise->set_value(true);
                        break;
                    }
                }
            }
        }

        /**
         * @brief Remove all read promises from the unreadAddPromises and unreadRemovePromises vectors.
         * If you want more fine grained control over the update process you can use this instead of the update function.
         * This function locks the promiseAddMutex and promiseRemoveMutex.
         * @note While this can be called during a forEach loop, it is unlikely to be useful there.
         */
        void cleanup() {
            std::scoped_lock lock{promiseAddMutex, promiseRemoveMutex};
            for (auto promise = unreadAddPromises.begin(); promise != unreadAddPromises.end(); promise++) {
                if ((*promise)->is_read()) {
                    promise = unreadAddPromises.erase(promise);
                }
            }
            unreadAddPromises.shrink_to_fit();

            for (auto promise = unreadRemovePromises.begin(); promise != unreadRemovePromises.end(); promise++) {
                if ((*promise)->is_read()) {
                    promise = unreadRemovePromises.erase(promise);
                }
            }
            unreadRemovePromises.shrink_to_fit();
        }

      public:
        /**
         * @brief Queue a value to be added to the container.
         * @param value The value to be added.
         * @param priority The priority of the value. lower values are added to the front of the array.
         * @return A promise of the ID of the value (needed to remove the value again).
         */
        std::shared_ptr<trackedFuture<uint64_t>> add(T value, int priority = 0) {
            std::scoped_lock lock{queueAddMutex};
            auto             promise = trackedPromise<uint64_t>::create();
            queuedAddValues.emplace_back(value, priority, promise);
            return promise->get_future();
        }

        /**
         * @brief Queue a value to be removed from the container.
         * @param id The id of the value to be removed.
         * @return A promise of a bool. The bool will be set to true if the value was removed false if it was not found.
         */
        std::shared_ptr<trackedFuture<bool>> remove(uint64_t id) {
            std::scoped_lock lock{queueRemoveMutex};
            auto             promise = trackedPromise<bool>::create();
            queuedRemoveIDs.emplace_back(id, promise);
            return promise->get_future();
        }

        /**
         * @brief Removes all values where the passes function returns true.
         * @param func The function to be called on each value.
         */
        void removeIf(std::function<bool(const T&)> func){
            std::scoped_lock lock{valueMutex};
            for (auto i = values.begin(); i < values.end(); i++) {
                if (func(std::get<0>(*i))) {
                    queuedRemoveIDs.emplace_back(std::get<2>(*i), trackedPromise<bool>::create());
                    values.erase(i);
                }
            }
        }

        /**
         * @brief Update the values vector.
         * This empties the queues and cleans up the promises.
         * @note This function locks all mutexes. Don't call it during a forEach loop or you will get a deadlock.
         */
        virtual void update() {
            addValues();
            removeValues();
            cleanup();
        }

        /**
         * @brief Run a function on each value in the container.
         * The function should return true if the loop should be stopped.
         * The function is called with a tuple containing the value, the priority and the ID.
         * @param func The function to run on each value.
         * @return true if the loop was stopped by the function, otherwise false.
         */
        bool forEachCancellable(std::function<bool(const std::tuple<T, int, uint64_t>&)> func) {
            std::shared_lock lock{valueMutex};
            for (const auto& value : values) {
                if (func(value)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @brief Run a function on each value in the container.
         * The function is called with a tuple containing the value, the priority and the ID.
         * @param func The function to be called on each value.
         */
        void forEach(std::function<void(const std::tuple<T, int, uint64_t>&)> func) {
            std::shared_lock lock{valueMutex};
            for (const auto& value : values) {
                func(value);
            }
        }

        /**
         * @brief Run a function on each value in the container.
         * The function should return true if the loop should be stopped.
         * The function is called with a value.
         * @param func The function to run on each value.
         * @return true if the loop was stopped by the function, otherwise false.
         */
        bool forEachCancellable(std::function<bool(T)> func) {
            std::shared_lock lock{valueMutex};
            for (auto [value, prio, id] : values) {
                if (func(value)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @brief Run a function on each value in the container.
         * The function is called with a value.
         * @param func The function to run on each value.
         */
        void forEach(std::function<void(T)> func) {
            std::shared_lock lock{valueMutex};
            for (auto [value, prio, id] : values) {
                func(value);
            }
        }

        /**
         * @brief Run a function on each value in the container and then clear the container.
         * @param func The function to run on each value.
         */
        void forEachOnce(std::function<void(T)> func) {
            std::shared_lock lock{valueMutex};
            for (auto [value, prio, id] : values) {
                func(value);
            }
            values.clear();
        }

        /**
         * @brief Move all values from the container to a new vector.
         * @note After this function the container will be empty.
         * @return The vector with all values.
         */
        std::vector<T> moveAll(){
            std::scoped_lock lock{valueMutex};
            std::vector<T> result;
            result.reserve(values.size());
            for (auto [value, prio, id] : values) {
                result.emplace_back(std::move(value));
            }
            values.clear();
            return result;
        }
    };
} // namespace SimpleGFX