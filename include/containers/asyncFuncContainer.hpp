#pragma once

#include "asyncValueContainer.hpp"
#include "concepts.hpp"
#include "stdAddons/trackedFuture.hpp"
#include <vector>

namespace SimpleGFX {

    namespace internal {
        /**
         * @brief A container that can be used to store functions asynchronously.
         * It contains data queues for adding and removing values, and a vector for storing the functions.
         * Functions can either be added using add or using addOnce;
         * Functions added using addOnce are removed after they are called.
         * The functions are called in the order of their priority.
         * The callAll function can be used to call all functions in the container.
         * The callAllVoid function can be used to call all functions in the container and discard their return values.
         *
         * @note This class is thread-safe.
         * @note This class is only intended to store functions. There is a separate class for storing values.
         * @tparam T The type of the values to store.
         */
        template <class function_type>
            requires std::invocable<function_type>
        class baseFuncContainer : protected asyncValueContainer<function_type> {
          private:
            /**
             * @brief The queue functions that will only be executed once
             * This is a separate queue because it is more efficient to store the functions in a separate vector.
             * This removes the need for an extra flag to mark the functions as "once".
             */
            std::vector<function_type> queuedOnce;

            /**
             * @brief The vector of functions that will only be executed once
             * These are the already registered functions that will only be executed once.
             * This array is split from the values array to make it more efficient to remove the functions after they are called.
             */
            std::vector<function_type> valuesOnce;

            // these are the mutexes for each vector
            std::shared_mutex queueAddOnceMutex;
            std::shared_mutex valueOnceMutex;

          protected:
            /**
             * Move the queue once functions to the values once vector.
             */
            void addFunctionsOnce() {
                std::scoped_lock lock{queueAddOnceMutex, valueOnceMutex};
                for (auto& func : queuedOnce) {
                    valuesOnce.push_back(func);
                }
                queuedOnce.clear();
            }

          public:
            /**
             * @brief Queue a function to be added to the container.
             * @param value The function to be added.
             * @param priority The priority of the function. lower priority function are added to the front of the array.
             * @return A promise of the ID of the function (needed to remove the function again).
             */
            std::shared_ptr<trackedFuture<uint64_t>> addFunc(function_type func, int priority = 0) {
                return std::move(asyncValueContainer<function_type>::add(std::move(func), priority));
            }

            /**
             * @brief Queue a function to be removed from the container.
             * @param id The id of the function to be removed.
             * @return A promise of a bool. The bool will be set to true if the function was removed false if it was not found.
             */
            std::shared_ptr<trackedFuture<bool>> removeFunc(uint64_t id) {
                return std::move(asyncValueContainer<function_type>::remove(id));
            }

            /**
             * @brief Queue a function to be added to the container once.
             * The function will removed once one of the call functions was called.
             * @param func The function to be added once
             */
            void addOnce(function_type func) {
                std::scoped_lock lock{queueAddOnceMutex};
                queuedOnce.push_back(std::move(func));
            }

            /**
             * @brief Update the values vectors.
             * This empties the queues and cleans up the promises.
             * @note This function locks all mutexes. Don't call it during a forEach loop or you will get a deadlock.
             */
            void update() {
                asyncValueContainer<function_type>::addValues();
                asyncValueContainer<function_type>::removeValues();
                addFunctionsOnce();
                asyncValueContainer<function_type>::cleanup();
            }

            /**
             * Call call functions and retrieve the results.
             * @note if you don't care about the return values use callAllVoid instead.
             * @param args The function arguments passed to all functions
             * @return std::vector<R> The results of the functions
             */
            template <class R, class... Args>
                requires std::invocable<function_type, Args...> && std::same_as<std::invoke_result_t<function_type, Args...>, R>
            [[nodiscard("use callAllVoid if you don't care about the return values")]]
            std::vector<R> callAll(Args... args) {
                std::vector<R> results;

                // call the functions in the values vector
                asyncValueContainer<function_type>::forEach(
                    [&](function_type func) { results.push_back(func(&args...)); });

                // call the functions in the valuesOnce vector
                std::scoped_lock lock{valueOnceMutex};
                for (auto& func : valuesOnce) {
                    results.push_back(func(&args...));
                }

                // clear the valuesOnce vector
                valuesOnce.clear();
                valuesOnce.shrink_to_fit();

                return std::move(results);
            }


            /**
             * @brief Calls all functions and discards their return value.
             * @param args The function arguments passed to all functions
             */
            template <class... Args>
                requires std::invocable<function_type, Args...>
            void callAllVoid(Args... args) {
                // call the functions in the values vector
                asyncValueContainer<function_type>::forEach([&](function_type func) { std::invoke(func, &args...); });

                // call the functions in the valuesOnce vector
                std::scoped_lock lock{valueOnceMutex};
                for (auto& func : valuesOnce) {
                    std::invoke(func, &args...);
                }

                // clear the valuesOnce vector
                valuesOnce.clear();
                valuesOnce.shrink_to_fit();
            }
        };
    }

    /**
     * @brief A container that can be used to store functions asynchronously.
     * It contains data queues for adding and removing values, and a vector for storing the functions.
     * Functions can either be added using add or using addOnce;
     * Functions added using addOnce are removed after they are called.
     * The functions are called in the order of their priority.
     * The callAll function can be used to call all functions in the container.
     * The callAllVoid function can be used to call all functions in the container and discard their return values.
     *
     * @note This class is thread-safe.
     * @note This class is only intended to store functions. There is a separate class for storing values.
     * @tparam T The type of the values to store.
     */
    template <class R, class... Args>
    class asyncFuncContainer : public internal::baseFuncContainer<std::function<R(Args...)>> {};

    /**
     * @brief A container that can be used to store functions asynchronously.
     * It contains data queues for adding and removing values, and a vector for storing the functions.
     * Functions can either be added using add or using addOnce;
     * Functions added using addOnce are removed after they are called.
     * The functions are called in the order of their priority.
     * The callAll function can be used to call all functions in the container.
     * The callAllVoid function can be used to call all functions in the container and discard their return values.
     *
     * @note This class is thread-safe.
     * @note This class is only intended to store functions. There is a separate class for storing values.
     * @tparam T The type of the values to store.
     */
    template <class R, class... Args>
    class secFuncContainer : public internal::baseFuncContainer<sec_sigc::weak_func_track_ptr<R, Args...>> {
      public:
        void addOnceT(const auto&... buildArgs) {
            this->addOnce(sec_sigc::weak_func_track_ptr<R, Args...>::create(buildArgs...));
        }

        std::shared_ptr<trackedFuture<uint64_t>> addFuncT(auto&... buildArgs, int priority = 0){
            return addFunc(sec_sigc::weak_func_track_ptr<R, Args...>::create(buildArgs...), priority);
        }
    };
} // namespace SimpleGFX