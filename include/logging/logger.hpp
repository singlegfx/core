#pragma once

#include "concepts.hpp"

#include <atomic>
#include <memory>
#include <shared_mutex>
#include <string>
#include <vector>
#include <mutex>
#include <iostream>
#include <sstream>
#include <chrono>

namespace SimpleGFX {
    /**
     * @brief This enum is used to define the logging level.
     *
     * Higher values mean more important messages.
     */
    enum loggingLevel { debug = -3, detail = -2, warning = -1, normal = 0, error = 1, fatal = 2 };

    /**
     * @brief Increase the logging level by one.
     * Each call will increase the level by one starting at debug and ending at fatal.
     * @param level The level to increase.
     * @return The increased level.
     */
    loggingLevel nextLevel(loggingLevel level);

    /**
     * @brief get the level name from a string
     * @note This is the inverse of logger::getLevelName
     * @note this ignores the case of the string
     * @param level the level string to convert
     * @throws std::invalid_argument if the string is not a valid level name
     * @return the logging level
     */
    loggingLevel levelFromString(const std::string& level);

    class logger;

    class logStream : public std::ostringstream {
      private:
        loggingLevel level;
        logger&      loggerPtr;

      public:
        logStream(loggingLevel _level, logger& _loggerPtr);

        ~logStream() override;
    };

    /**
     * @brief This is the default logger class. It is used to log messages and exceptions.
     *
     * It logs messaged to cout or cerr depending on the logging level.
     *
     */
    class logger {
      private:
        logger() = delete;

        /**
         * @brief The mutex used to lock the extraLoggers vector.
         */
        std::shared_mutex extraLoggerMutex;

        /**
         * @brief A vector of extra loggers.
         *
         * This is used to log messages to other loggers.
         */
        std::vector<std::shared_ptr<logger>> extraLoggers;

      protected:
        /**
         * @brief Only messages with a level higher or equal to this will be logged.
         */
        std::atomic<loggingLevel> displayedLevel = debug;

        /**
         * @brief If true, the level will be displayed before the message.
         */
        std::atomic<bool> showLevel = true;

        /**
         * @brief If true, the time will be displayed before the message.
         */
        std::atomic<bool> showTime = true;

        /**
         * @brief The mutex used to lock the output buffer.
         *
         * This should be used to make logLine thread safe and prevent messages from being mixed up.
         */
        std::shared_mutex outputBufferMutex;

        /**
         * @brief Log a message as a line.
         * @param message The message to log.
         * @param level The level of the message.
         */
        virtual void logLine(const std::string& message, loggingLevel level);

        /**
         * @brief Log a message to the extra loggers.
         * @param message The message to log.
         * @param level The level of the message.
         */
        void logExtras(const std::string& message, loggingLevel level);

      public:
        /**
         * @brief Construct a new logger object
         * @param _displayLevel The lowest level of messages that will be logged.
         */
        explicit logger(loggingLevel _displayLevel, bool _showLevel = true, bool _showTime = true);

        /**
         * @brief Destroy the logger object
         */
        virtual ~logger() = default;

        /**
         * @brief Log a message with a given level.
         * @param message The message to log.
         * @param level The level of the message.
         */
        void logMessage(const std::string& message, loggingLevel level = normal);

        /**
         * @brief log a message by using the stream operator.
         * This operator will log the message as soon as the stream is finished.
         * The interface is similar to std::cout or std::cerr.
         */
        SimpleGFX::logStream operator<<(loggingLevel level);

        /**
         * @brief Log an exception as a line.
         * @note This will use the helper::decodeException function to decode the exception.
         * @param e The exception to log.
         * @param isFatal If true, the exception will be logged as fatal otherwise as error.
         */
        void logException(const std::exception& e, bool isFatal = false);

        /**
         * @brief get the current exception and log it.
         * @param isFatal If true, the exception will be logged as fatal otherwise as error.
         * @note This will use the helper::decodeException function to decode the exception.
         * @note Call this function in a catch block to log the current exception.
         */
        void logCurrrentException(bool isFatal = false);

        /**
         * @brief Add a logger that will also receive all log messages.
         * @param logger The logger to add.
         */
        void addExtraLogger(std::shared_ptr<logger> logger);

        /**
         * @brief Remove a logger from the extra loggers.
         * @param logger The logger to remove.
         */
        void removeExtraLogger(std::shared_ptr<logger> logger);

        /**
         * @brief Turn a loggingLevel into a string.
         * @param level The level to convert.
         * @return The string representation of the level.
         */
        static std::string getLevelName(loggingLevel level) {
            switch (level) {
                case debug:
                    return "DEBUG";
                case detail:
                    return "DETAIL";
                case warning:
                    return "WARNING";
                case normal:
                    return "NORMAL";
                case error:
                    return "ERROR";
                case fatal:
                    return "FATAL";
                default:
                    return "UNKNOWN";
            }
        };

        /**
         * @brief Get the current timestamp as a string.
         * @note This will use the local time.
         * @details The default format is "%Y-%m-%d %H:%M:%S".
         * If fs_safe is true, the format will be "%Y-%m-%d_%H-%M-%S".
         * @param fs_safe If true, the timestamp will be formatted in a way that it can be used as a filename.
         * @return The current timestamp as a string.
         */
        static std::string getCurrentTimestamp(bool fs_safe = false) {
            const char*       format_normal  = "%Y-%m-%d %H:%M:%S";
            const char*       format_fs_safe = "%Y-%m-%d_%H-%M-%S";
            auto              now            = std::chrono::system_clock::now();
            auto              now_c          = std::chrono::system_clock::to_time_t(now);
            std::stringstream ss;

            ss << std::put_time(std::localtime(&now_c), fs_safe ? format_fs_safe : format_normal);
            return ss.str();
        }
    };

} // namespace SimpleGFX
