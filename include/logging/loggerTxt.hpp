#pragma once

#include "logger.hpp"

#include <filesystem>

namespace SimpleGFX {
    class loggerTxtProperties {
      public:
        loggingLevel          displayLevel = normal;
        std::filesystem::path logPath;
        bool                  appendFile = true;
        bool                  cleanFile = false;
        bool                  showLevel = true;
        bool                  showTime = true;

        loggerTxtProperties(loggingLevel _displayLevel, const std::filesystem::path& _logPath);
    };

    class loggerTxt : public logger {
      protected:
        /**
         * @brief Log a message as a line.
         * @param message The message to log.
         * @param level The level of the message.
         */
        virtual void logLine(const std::string& message, loggingLevel level) override;

        /**
         * @brief The file to log to.
         */
        std::filesystem::path logPath;

        /**
         * @brief If true, the file will be overwritten on every Message.
         * If false, the file will be appended to.
         */
        std::atomic<bool> overrideFile = false;

      public:
        /**
         * @brief Construct a new loggerTxt object
         * @param _displayLevel The lowest level of messages that will be logged.
         */
        loggerTxt(loggingLevel _displayLevel, const std::filesystem::path& _logPath);

        loggerTxt(const loggerTxtProperties& props);
    };
} // namespace SimpleGFX
