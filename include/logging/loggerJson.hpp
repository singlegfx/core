#pragma once

#include "logger.hpp"

#include "json/jsonUtils.hpp"

#include <filesystem>

namespace SimpleGFX {

    /**
     * @brief A logger that logs to a JSON file.
     *
     * @details This logger logs to a JSON file, which can be used to analyze the log data.
     * This is most useful when adding this as an extra logger to another logger.
     */
    class loggerJson : public logger {
      protected:
        /**
         * @brief Log a message as a line.
         * @param message The message to log.
         * @param level The level of the message.
         */
        void logLine(const std::string& message, loggingLevel level) override;

        /**
         * @brief The raw underlying JSON data.
         */
        nlohmann::json jsonData;

        /**
         * @brief The path to the log file.
         */
        std::filesystem::path logPath;

        /**
         * @brief If true, the log file will be updated every time a message is logged.
         *
         * @details If this is false, the log file will only be updated when the logger is destroyed.
         */
        std::atomic<bool> updateEveryMessage = true;

      public:
        /**
         * @brief Construct a new loggerJson object.
         *
         * @param _displayLevel The level of messages that will be displayed.
         * @param _logPath The path to the log file.
         */
        loggerJson(loggingLevel _displayLevel, std::filesystem::path _logPath, bool _updateEveryMessage = true);

        /**
         * @brief Destroy the loggerJson object.
         */
        ~loggerJson() override;
    };

} // namespace SimpleGFX
