project(
    'SimpleGFX',
    'cpp',
    license: 'LGPL-3.0',
    default_options : [
        'cpp_std=c++23',
        'warning_level=3'
    ],
    version : '0.1.0',
    meson_version : '>= 1.0.0',
)

# define the correct export macro on windows
if target_machine.system() == 'windows'
    if get_option('buildtype') != 'static'
        add_project_arguments('-DSIMPLEGFX_EXPORT_MACRO=__declspec(dllexport)', language: 'cpp')
    else
        add_project_arguments('-DSIMPLEGFX_EXPORT_MACRO=__declspec(dllimport)', language: 'cpp')
    endif
endif

CC = meson.get_compiler('cpp')
if CC.has_argument('-Werror=multichar')
    add_project_arguments('-Werror=multichar', language: 'cpp')
endif

sources = [
    'src/events/eventSystem.cpp',

    'src/json/jsonUtils.cpp',

    'src/stdAddons/stringUtils.cpp',
    'src/stdAddons/filesystemUtils.cpp',
    'src/stdAddons/exceptionUtils.cpp',
    'src/stdAddons/chronoUtils.cpp',

    'src/logging/logger.cpp',
    'src/logging/loggerJson.cpp',
    'src/logging/loggerTxt.cpp',
]

simplegfx_deps = [
    dependency('dl', required: false),
    dependency('sec_sigc++'),
    dependency('nlohmann_json'),
    dependency('threads'),
]


#include all headers including the video configuration
incdir = include_directories('include')

#define the video component
simplegfx = library(
    'simplegfx', 
    sources, 
    version : meson.project_version(), 
    soversion : '0',
    include_directories : incdir,
    dependencies: simplegfx_deps,
    install : true,
)

simplegfx_dep = declare_dependency(
    include_directories : incdir,
    link_with : simplegfx,
    dependencies : simplegfx_deps,
    version: meson.project_version()
)
meson.override_dependency('simplegfx', simplegfx_dep)
meson.override_dependency('SimpleGFX', simplegfx_dep)
meson.override_dependency('simpleGFX', simplegfx_dep)

install_subdir('include', install_dir : 'include/simplegfx', strip_directory : true)

pkg = import('pkgconfig')
pkg.generate(simplegfx)

ADD_TESTS = get_option('build_tests').enabled()
if get_option('build_tests').auto()
    ADD_TESTS = not meson.is_subproject()
endif

if ADD_TESTS
    subdir('tests')
endif
