using namespace SimpleGFX;

template <typename T, typename C>
static int64_t cmpTSwitch(const T& value, const C& cases) {
    return TSwitch<T, C>(value, cases, [](const auto& a, const auto& b){return a == b;});
}

template <typename T, typename C>
static int64_t roughlyTSwitch(const T& value, const C& cases) {
    return TSwitch<T, C>(value, cases, [](const auto& a, const auto& b){return numbers::isRoughly(a, b);});
}

/**
 * This test checks if the TSwitch function works as expected.
 * It checks the TSwitch for int types, and three types of containers: std::vector, std::array and std::initializer_list.
 * The value check is done using the simpleCompare function, cmpTSwitch and roughlyTSwitch.
 * The other two function are wrappers that use custom compare functions.
 */
TEST(stdAddons, TSwitchInt) {
    std::vector<int> casesVec = {0, 1, 2, 3, 4};
    std::array<int, 5> casesArr = {0, 1, 2, 3, 4};
    auto casesList = {0, 1, 2, 3, 4};

    for(int i = 0; i < 5; i++){
        auto res = static_cast<int64_t>(i);

        EXPECT_EQ(TSwitch(i, casesVec), res);
        EXPECT_EQ(TSwitch(i, casesArr), res);
        EXPECT_EQ(TSwitch(i, casesList), res);

        EXPECT_EQ(TSwitch(static_cast<unsigned int>(i), casesVec), res);
        EXPECT_EQ(TSwitch(static_cast<unsigned int>(i), casesArr), res);
        EXPECT_EQ(TSwitch(static_cast<unsigned int>(i), casesList), res);

        EXPECT_EQ(cmpTSwitch(i, casesVec), res);
        EXPECT_EQ(cmpTSwitch(i, casesArr), res);
        EXPECT_EQ(cmpTSwitch(i, casesList), res);

        EXPECT_EQ(cmpTSwitch(static_cast<unsigned int>(i), casesVec), res);
        EXPECT_EQ(cmpTSwitch(static_cast<unsigned int>(i), casesArr), res);
        EXPECT_EQ(cmpTSwitch(static_cast<unsigned int>(i), casesList), res);

        EXPECT_EQ(roughlyTSwitch(i, casesVec), res);
        EXPECT_EQ(roughlyTSwitch(i, casesArr), res);
        EXPECT_EQ(roughlyTSwitch(i, casesList), res);

        EXPECT_EQ(roughlyTSwitch(static_cast<unsigned int>(i), casesVec), res);
        EXPECT_EQ(roughlyTSwitch(static_cast<unsigned int>(i), casesArr), res);
        EXPECT_EQ(roughlyTSwitch(static_cast<unsigned int>(i), casesList), res);
    }

    EXPECT_EQ(TSwitch(5, casesVec), -1);
    EXPECT_EQ(TSwitch(5, casesArr), -1);
    EXPECT_EQ(TSwitch(5, casesList), -1);

    EXPECT_EQ(cmpTSwitch(5, casesVec), -1);
    EXPECT_EQ(cmpTSwitch(5, casesArr), -1);
    EXPECT_EQ(cmpTSwitch(5, casesList), -1);

    EXPECT_EQ(roughlyTSwitch(5, casesVec), -1);
    EXPECT_EQ(roughlyTSwitch(5, casesArr), -1);
    EXPECT_EQ(roughlyTSwitch(5, casesList), -1);
}


TEST(stdAddons, TSwitchDouble) {
    std::vector<double> casesVec = {0.0, 1.0, 2.0, 3.0, 4.0};
    std::array<double, 5> casesArr = {0.0, 1.0, 2.0, 3.0, 4.0};
    auto casesList = {0.0, 1.0, 2.0, 3.0, 4.0};

    for(double i = 0; i < 4.9; i+=1.0){
        auto res = static_cast<int64_t>(i);

        EXPECT_EQ(roughlyTSwitch(i, casesVec), res);
        EXPECT_EQ(roughlyTSwitch(i, casesArr), res);
        //EXPECT_EQ(roughlyTSwitch(i, casesList), res);

        EXPECT_EQ(roughlyTSwitch(static_cast<float>(i), casesVec), res);
        EXPECT_EQ(roughlyTSwitch(static_cast<float>(i), casesArr), res);
        //EXPECT_EQ(roughlyTSwitch(static_cast<float>(i), casesList), res);
    }

    EXPECT_EQ(roughlyTSwitch(5.0, casesVec), -1);
    EXPECT_EQ(roughlyTSwitch(5.0, casesArr), -1);
    EXPECT_EQ(roughlyTSwitch(5.0, casesList), -1);
}

