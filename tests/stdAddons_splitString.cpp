using namespace SimpleGFX;

TEST(stdAddons, SplitString_OneDelimiter) {
    std::string input = "apple,banana,orange";
    char delimiter = ',';
    std::vector<std::string> expected_output = {"apple", "banana", "orange"};
    EXPECT_EQ(string::splitString(input, delimiter), expected_output);
}

TEST(stdAddons, SplitString_MultipleDelimiters) {
    std::string input = "one::two:::three";
    char delimiter = ':';
    std::vector<std::string> expected_output = {"one", "", "two", "", "", "three"};
    EXPECT_EQ(string::splitString(input, delimiter), expected_output);
}

TEST(stdAddons, SplitString_EmptyString) {
    std::string input = "";
    char delimiter = ',';
    std::vector<std::string> expected_output = {};
    EXPECT_EQ(string::splitString(input, delimiter), expected_output);
}

TEST(stdAddons, SplitString_NoDelimiters) {
    std::string input = "hello world";
    char delimiter = '/';
    std::vector<std::string> expected_output = {"hello world"};
    EXPECT_EQ(string::splitString(input, delimiter), expected_output);
}

TEST(stdAddons, SplitString_EscapeCharacters) {
    std::string input = "apple\\,banana,orange";
    char delimiter = ',';
    std::vector<std::string> expected_output = {"apple\\", "banana", "orange"};
    EXPECT_EQ(string::splitString(input, delimiter), expected_output);
}
