using namespace SimpleGFX;

template <typename T, size_t SIZE>
void test_append_value(std::array<T, SIZE>&& original, const std::array<T, SIZE>& appended, T value) {
    auto original_copy = std::move(original);
    container::appendValue(original_copy, value);

    for (size_t i = 0; i < SIZE; i++) {
        EXPECT_EQ(original_copy[i], appended[i]);
    }
}

TEST(stdAddons, appendValue) {
    test_append_value<int, 5>({1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, 6);
    test_append_value<unsigned int, 5>({1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, 6);
    test_append_value<float, 5>({1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, 6);
    test_append_value<double, 5>({1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, 6);
}

TEST(stdAddons, contains){
    std::array<int, 5> dataArr = {1, 2, 3, 4, 5};
    std::vector<int> dataVec = {1, 2, 3, 4, 5};

    for(int i = 1; i <= 5; i++){
        EXPECT_TRUE(std::ranges::contains(dataArr, i));
        EXPECT_TRUE(std::ranges::contains(dataVec, i));
    }

    for(unsigned int i = 1; i <= 5; i++){
        EXPECT_TRUE(std::ranges::contains(dataArr, i));
        EXPECT_TRUE(std::ranges::contains(dataVec, i));
    }

    EXPECT_FALSE(std::ranges::contains(dataArr, 6));
    EXPECT_FALSE(std::ranges::contains(dataVec, 6));
    EXPECT_FALSE(std::ranges::contains(dataArr, 0));
    EXPECT_FALSE(std::ranges::contains(dataVec, 0));
}

//these are supposed to fail to compile
//uncomment the tests below to verify after making
//changes to the concept or appendValue function
/*
TEST(stdAddons, appendValue_size0) {
    std::array<int, 0> original = {};
    test_append_value<int, 0>({}, {}, 2);
    container::appendValue(original, 0);
}
*/
