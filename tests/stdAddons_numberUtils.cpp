
using namespace SimpleGFX;

TEST(stdAddons, number_isRoughly) {
    EXPECT_FALSE(numbers::isRoughly<double>(1.0, -1.0)) << "1.0 and -1.0 are reported roughly equal";
    EXPECT_FALSE(numbers::isRoughly<double>(1.01, 1.00)) << "1.01 and 1.00 are reported roughly equal";
    EXPECT_TRUE(numbers::isRoughly<double>(1.001, 1.000)) << "1.001 and 1.000 are reported not roughly equal";
    EXPECT_TRUE(numbers::isRoughly<int>(1001, 1000)) << "1001 and 1000 are reported not roughly equal";
};