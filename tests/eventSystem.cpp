
using namespace sec_sigc;
using namespace SimpleGFX;
using namespace std::literals;

class dummyPoller : public tracked_eventPoller {
  public:
    dummyPoller() = default;
    void operator()(eventManager& manager) {
        inputEvent testEvent{
            .name       = "testEvent",
            .originName = "dummyPoller",
            .amount     = 1,
            .inputType  = press,
        };
        auto fut = manager.raiseEvent(testEvent);
        fut->drop();
    }
};

class dummyHandle : public tracked_eventHandle {
  public:
    dummyHandle() = default;
    void operator()(const inputEvent& event, bool& handled) {
        ASSERT_FALSE(handled);
        ASSERT_EQ(event.name, "testEvent");
        ASSERT_EQ(event.originName, "dummyPoller");
        ASSERT_EQ(event.inputType, press);
        handled = event.amount > 0;
    }
};

TEST(eventSystem, simpleDummyTest) {
    eventManager manager;
    dummyPoller  poller;
    dummyHandle  handler;
    manager.registerPoller(poller);
    manager.registerHandler(handler);

    manager.poll();
    auto fut_true = manager.raiseEvent(inputEvent{
        .name       = "testEvent",
        .originName = "dummyPoller",
        .amount     = 2,
        .inputType  = press,
    });
    auto fut_false = manager.raiseEvent(inputEvent{
        .name       = "testEvent",
        .originName = "dummyPoller",
        .amount     = 0,
        .inputType  = press,
    });

    fut_true->wait();
    ASSERT_TRUE(fut_true->get());
    fut_false->wait();
    ASSERT_FALSE(fut_false->get());
}
