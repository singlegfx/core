using namespace SimpleGFX;

TEST(stdAddons, exception_decode_level0) {
    // Normal Exception
    try {
        throw std::runtime_error("Test Error");
    } catch (const std::exception& e) {
        testing::internal::CaptureStderr();
        exception::printException(e);
        std::string output = testing::internal::GetCapturedStderr();
        EXPECT_EQ(output, "exception: Test Error\n");
        
        std::stringstream ss{};
        exception::decodeException(e, ss);
        EXPECT_EQ(ss.str(), "exception: Test Error\n");
    }
}

TEST(stdAddons, exception_decode_level1) {
    // Nested Exception
    try {
        try {
            throw std::runtime_error("Nested Test Error");
        } catch (...) {
            std::throw_with_nested(std::runtime_error("Test Error"));
        }
    } catch (const std::exception& e) {
        testing::internal::CaptureStderr();
        exception::printException(e);
        std::string output = testing::internal::GetCapturedStderr();
        EXPECT_EQ(output, "exception: Test Error\n exception: Nested Test Error\n");
        
        std::stringstream ss{};
        exception::decodeException(e, ss);
        EXPECT_EQ(ss.str(), "exception: Test Error\n exception: Nested Test Error\n");
    }
}

TEST(stdAddons, exception_decode_level2) {
    // Nested Exception
    try {
        try {
            throw std::runtime_error("Nested Test Error");
        } catch (...) {
            try {
                std::throw_with_nested(std::runtime_error("Nested Test Error"));
            } catch (...) {
                std::throw_with_nested(std::runtime_error("Test Error"));
            }
        }
    } catch (const std::exception& e) {
        testing::internal::CaptureStderr();
        exception::printException(e);
        std::string output = testing::internal::GetCapturedStderr();
        EXPECT_EQ(output, "exception: Test Error\n exception: Nested Test Error\n  exception: Nested Test Error\n");
        
        std::stringstream ss{};
        exception::decodeException(e, ss);
        EXPECT_EQ(ss.str(), "exception: Test Error\n exception: Nested Test Error\n  exception: Nested Test Error\n");
    }
}

