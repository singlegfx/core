# Simple GFX core

This project contains a support library for several all of the projects in this project.
The goal is to implement common functions/features once and use them throughout all projects.

## Usage information

Just add a wrap file for this repository and and add it as meson dependency.
Now all you have to do is include the header with `#include <simplegfx.hpp>` and you are ready to go.

All functions are part of the `helper` class in the `SimpleGFX` namespace and defined as static.
So you can use them like this:

```cpp
#include <simplegfx_helper.hpp>

int main(){
    std::vector<int> myVec {1,2,3,5,6};
    
    if(SimpleGFX::helper::contains<int>(myVec, 4)){
        std::cout << "vector contains the 4" << std::endl;
    }else{
        std::cout << "vector does not contain the 4" << std::endl;
    }
    
    return 0;
}

```
